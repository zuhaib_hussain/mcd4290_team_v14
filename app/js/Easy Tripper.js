"use strict";

function showSummary() {
    let trip = getDataLocalStorageTrip()

    let country_pointer = document.getElementById("countries")
    trip._country = country_pointer.value;
    let summaryCountry = `Country: <br> ${trip._country}`
    let summaryCountryRef = document.getElementById("summary-country")
    summaryCountryRef.innerHTML = summaryCountry;

    let dateRef = document.getElementById("date")
    trip._date = dateRef.value;
    let summaryDate = `Date: <br> ${trip._date}`
    let summaryDateRef = document.getElementById("summary-date")
    summaryDateRef.innerHTML = summaryDate;

    let summaryStart = `Start: <br> ${trip._start}`
    let summaryStartRef = document.getElementById("summary-start")
    summaryStartRef.innerHTML = summaryStart;

    let summaryEnd = `End: <br> ${trip._end}`
    let summaryEndRef = document.getElementById("summary-end")
    summaryEndRef.innerHTML = summaryEnd;

    let summaryStops = `Stops: <br> ${trip._stops}`
    let summaryStopsRef = document.getElementById("summary-stops")
    summaryStopsRef.innerHTML = summaryStops;

    if (trip._routes.length > 0) {
        trip._start = trip._routes[0]._start.name;
        trip._end = trip._routes[trip._routes.length - 1]._end.name;
        trip._stops = trip._routes.length - 1;

        let routeRef = document.getElementById("routeLists")
        let output = "";
        for (let i = 0; i < trip._routes.length; i++) {
            output += `<tr>
    <td class="mdl-data-table__cell--non-numeric"><span class="mdl-list__item-primary-content">
    <i class="material-icons material-icons-large material-icons-summary mdl-list__item-avatar">flight</i></span></td>
    <td><span>${trip._routes[i]._start.name}-${trip._routes[i]._end.name}</span></td>
    <td class="mdl-data-table__cell--date">
    <a class="mdl-list__item-secondary-action" href="#" onclick="removeRoute(${i})"><i class="material-icons">close</i></a></td>
    </tr>`
        }
        routeRef.innerHTML = output;
    }
    updateLocalStorageTrip(trip);
}


function removeRoute(i) {
    let trip = getDataLocalStorageTrip()
    trip._routes.splice(i, trip._routes.length - i)
    updateLocalStorageTrip(trip)
    localStorage.setItem(COUNTRY_KEY, country)
    window.location = "Easy Tripper.html"
}


function finishTrip() {
    let dateRef = document.getElementById("date");
    if (dateRef.value == undefined || dateRef.value == "" || dateRef.value == null) {
        alert("Please select a date for your trip");
    } else {
        let trip = getDataLocalStorageTrip();
        if (getDataLocalStorageUser() != undefined && getDataLocalStorageUser() != null && getDataLocalStorageUser() != "") {
            let user;
            for (let i = 0; i < accounts._accounts.length; i++) {
                user = getDataLocalStorageUser();
                let emailId = user._emailId;
                if (accounts._accounts[i]._emailId == emailId) {
                    user = accounts._accounts[i];
                    updateLocalStorageUser(user);
                }
            }
            if (getDataLocalStorageTrip() != undefined && getDataLocalStorageTrip() != null) {
                user.addTrip(trip._tripId);
                user.getTripIndex(user._trips.length - 1)._country = trip._country;
                user.getTripIndex(user._trips.length - 1)._date = trip._date;
                user.getTripIndex(user._trips.length - 1)._start = trip._start;
                user.getTripIndex(user._trips.length - 1)._end = trip._end;
                user.getTripIndex(user._trips.length - 1)._stops = trip._stops;
                user.getTripIndex(user._trips.length - 1)._routes = trip._routes;
                trip = "";
                updateLocalStorageTrip(trip);
            }
            updateLocalStorageUser(user);
            updateLocalStorageAccount(accounts);
            window.location = "scheduledTrips.html";
        } else {
            updateLocalStorageTrip(trip);
            window.location = "login.html";
        }
    }
}