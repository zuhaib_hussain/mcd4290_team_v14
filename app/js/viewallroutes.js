"use strict";


let country_pointer = document.getElementById("countries");
let country_list_fixed = "";

for (let i = 0; i < countryData.length; i++) {
    country_list_fixed += `<option value="${countryData[i]}">${countryData[i]}</option>`;
}

country_pointer.innerHTML = country_list_fixed;

let countryStorage = localStorage.getItem(COUNTRY_KEY);
let country = "";

if (countryStorage != undefined) {
    country_pointer.value = countryStorage;
    country = country_pointer.value;
}

mapboxgl.accessToken = "pk.eyJ1IjoidGVhbS0wOTQiLCJhIjoiY2tldXZtOXd2MWhxNDJ1b21kMTVqc2oxZCJ9.boFRONkPtgOP_E-VqtDMlQ";
let airports = [];
let routes = [];
let coords = [];
let startAirport = [];
let endAirport = [];
let routeDetails = [];
let markers = [];


let map = new mapboxgl.Map({
    container: 'map',
    center: [144.9648731, -37.8182711],
    zoom: 4,
    style: 'mapbox://styles/mapbox/streets-v9'
});


function request_web_service(url, data) {
    let params = "";
    for (let key in data) {
        if (data.hasOwnProperty(key)) {
            if (params.length == 0) {
                params += "?";
            } else {
                params += "&";
            }
            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);
            params += encodedKey + "=" + encodedValue;
        }
    }
    let script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
}


function fetch_airports_data() {
    country = country_pointer.value;
    let url = "https://eng1003.monash/OpenFlights/airports/";
    let data = {
        country: `${country}`,
        callback: "showAirports"
    }
    request_web_service(url, data);
}


function showAirports(result) {
    airports = result;
    removeMarkers();
    map.panTo([airports[0].longitude, airports[0].latitude]);

    for (let i = 0; i < airports.length; i++) {
        let marker = new mapboxgl.Marker({
            "color": "rgb(255,193,7)"
        });
        markers.push(marker);
        let latitude = airports[i].latitude;
        let longitude = airports[i].longitude;
        let coordinates = [longitude, latitude];
        marker.setLngLat(coordinates);
        marker.addTo(map);
        let description = `<h3>Airport: ${airports[i].name}<br></h3> 
            <h3>City: ${airports[i].city} <h3>`;
        let popup = new mapboxgl.Popup({
            offset: 45
        });
        popup.setHTML(description);
        marker.setPopup(popup);
    }
    getAllRouteData();
}


function getAllRouteData() {
    let url = "https://eng1003.monash/OpenFlights/allroutes/";
    let data = {
        country: `${country}`,
        callback: "showAllRoutes"
    }
    request_web_service(url, data);
}

function showAllRoutes(result) {

    routes = [];
    coords = [];
    let startCoords = [];
    let endCoords = [];
    for (let i = 0; i < result.length; i++) {
        for (let j = 0; j < airports.length; j++) {

            if (result[i].sourceAirportId == airports[j].airportId) {
                startCoords = [airports[j].longitude, airports[j].latitude];
                startAirport = airports[j];
            }
            if (result[i].destinationAirportId == airports[j].airportId) {
                endCoords = [airports[j].longitude, airports[j].latitude];
                endAirport = airports[j];
                routeDetails = result[i];
            }
        }


        if (startCoords.length != 0 && endCoords.length != 0) {
            coords.push(startCoords);
            coords.push(endCoords);
            routes.push(result[i]);
        }
        removeLayerWithId("routes");
        showPath();
    }

}


function removeLayerWithId(idToRemove) {
    let hasPoly = map.getLayer(idToRemove);
    //?
    if (hasPoly !== undefined) {
        map.removeLayer(idToRemove);
        map.removeSource(idToRemove);
    }
}


function showPath() {
    let object = {
        type: "geojson",
        data: {
            type: "Feature",
            properties: {},
            geometry: {
                type: "LineString",
                coordinates: []
            }
        }
    };
    for (let j = 0; j < coords.length; j++) {
        object.data.geometry.coordinates.push(coords[j]);
    }

    map.addLayer({
        id: "routes",
        type: "line",
        source: object,
        layout: {
            "line-join": "round",
            "line-cap": "round"
        },
        paint: {
            "line-color": "#888",
            "line-width": 6
        }
    });
}


function planPage() {
    country = country_pointer.value;
    localStorage.setItem(COUNTRY_KEY, country);
    let userTrip = new Trip("0");
    updateLocalStorageTrip(userTrip);
    window.location = "Easy Tripper.html"; //
}


function removeMarkers() {
    if (markers.length != 0) {
        for (let i = 0; i < markers.length; i++) {
            markers[i].remove();
        }
    }
}