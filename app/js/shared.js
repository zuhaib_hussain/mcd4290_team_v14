"use strict";

const ACCOUNTS_DATA_KEY = "accountLocalData";
const USER_DATA_KEY = "userLocalData";
const TRIP_DATA_KEY = "tripLocalData";
const AIRPORTS_DATA_KEY = "airportLocalData";
const COUNTRY_KEY = "countryLocalData";

class Route {

    constructor(routeId) {
        this._routeId = routeId;
        this._start = "";
        this._end = "";
        this._equipment = "";
        this._airline = "";
    }


    get routeId() {
        return this._routeId;
    }

    get start() {
        return this._start;
    }

    get end() {
        return this._end;
    }

    get equipment() {
        return this._equipment;
    }

    get airline() {
        return this._airline;
    }


    set routeId(id) {
        this._routeId = id;
    }

    set start(sourceAirport) {
        this._start = sourceAirport;
    }

    set end(destinationAirport) {
        this._end = destinationAirport;
    }

    set equipment(type) {
        this._equipment = type;
    }

    set airline(text) {
        this._airline = text;
    }


    fromData(data) {
        this._routeId = data.routeId;
        this._start = data._start;
        this._end = data._end;
        this._airline = data._airline;
        this._equipment = data._equipment;
    }
}
class Trip {

    constructor(tripId) {
        this._tripId = tripId;
        this._date = "";
        this._country = "";
        this._routes = [];
        this._start = "";
        this._end = "";
        this._stops = 0;
    }


    get tripId() {
        return this._tripId;
    }

    get date() {
        return this._date;
    }

    get country() {
        return this._country;
    }

    get routes() {
        return this._routes;
    }

    get start() {
        return this._start;
    }

    get end() {
        return this._end;
    }

    get stops() {
        return this._stops;
    }

    set tripId(id) {
        this._tripId = id;
    }

    set date(dateType) {
        this._date = dateType;
    }

    set country(countryType) {
        this._country = countryType;
    }

    set routes(array) {
        this._routes = array;
    }

    set start(data) {
        this._start = data;
    }

    set end(data) {
        this._end = data;
    }

    set stops(data) {
        this._stops = data;
    }


    getRouteIndex(index) {
        return this._routes[index];
    }


    addRoute(routeId) {
        let route = new Route(routeId);
        this._routes.push(route);
        this._stops = this._routes.length - 1; //?
    }


    removeRoute(routeId) {
        for (let i = 0; i < this._routes.length; i++) {
            if (this._routes[i].routeId == routeId) {
                this._routes.splice(i, 1); //there is only 1 item to remove from routes array.

            }
        }
    }


    fromData(data) {
        let dataValues = data._routes;
        this._routes = [];
        this._tripId = data._tripId;
        this._date = data._date;
        this._country = data._country;
        this._start = data._start;
        this._end = data._end;
        this._stops = data._stops;
        for (let i = 0; i < dataValues.length; i++) {
            let route = new Route();
            route.fromData(dataValues[i]);
            this._routes.push(route);
        }
    }
}

class Account {

    constructor(emailId) {
        this._emailId = emailId;
        this._name = ""; //must be added
        this._password = ""; //must be added
        this._trips = [];
    }


    get name() {
        return this._name;
    }

    get emailId() {
        return this._emailId;
    }

    get password() {
        return this._password;
    }

    get trips() {
        return this._trips;
    }

    set name(newName) {
        this._name = newName;
    }

    set emailId(id) {
        this._emailId = id;
    }

    set password(newPassword) {
        this._password = newPassword;
    }

    set trips(newTrip) {
        this._trips = newTrip;
    }


    getTripIndex(index) {
        return this._trips[index];
    }


    addTrip(tripId) {
        let trip = new Trip(tripId);
        this._trips.push(trip);
    }


    removeTrip(i) {

        this._trips.splice(i, 1);
    }


    fromData(data) {
        let dataValues = data._trips;
        this._trips = [];
        this._name = data._name;
        this._emailId = data._emailId;
        this._password = data._password;

        for (let i = 0; i < dataValues.length; i++) {
            let trip = new Trip();
            trip.fromData(dataValues[i]);
            this._trips.push(trip);
        }
    }
}
class AccountList {
    constructor() {
        this._accounts = [];
    }

    get accounts() {
        return this._accounts;
    }

    set accounts(data) {
        this._accounts = data;
    }

    get count() {
        return this._accounts.length;
    }

    getAccount(index) {
        return this._accounts[index];
    }

    addAccount(emailId) {
        let account = new Account(emailId);
        this._accounts.push(account);
    }


    fromData(data) {
        let dataValues = data._accounts;
        this._accounts = [];
        for (let i = 0; i < dataValues.length; i++) {
            let account = new Account();
            account.fromData(dataValues[i]);
            this._accounts.push(account);
        }
    }
}



function checkIfAccountDataExistsLocalStorage() {
    let data = localStorage.getItem(ACCOUNTS_DATA_KEY);
    if (data && data !== null && data !== undefined && data !== "") {
        return true;
    } else {
        return false;
    }
}



function updateLocalStorageAccount(data) {
    let jsonString = JSON.stringify(data);
    localStorage.setItem(ACCOUNTS_DATA_KEY, jsonString);
}


function updateLocalStorageUser(data) {
    let jsonString = JSON.stringify(data);
    localStorage.setItem(USER_DATA_KEY, jsonString);
}


function updateLocalStorageTrip(data) {
    let jsonString = JSON.stringify(data);
    localStorage.setItem(TRIP_DATA_KEY, jsonString);
}


function updateLocalStorageAirports(data) {
    let jsonString = JSON.stringify(data);
    localStorage.setItem(AIRPORTS_DATA_KEY, jsonString);
}


function getDataLocalStorageAccount() {
    let jsonString = localStorage.getItem(ACCOUNTS_DATA_KEY);
    let data = JSON.parse(jsonString);
    return data;
}


function getDataLocalStorageUser() {
    let jsonString = localStorage.getItem(USER_DATA_KEY);
    let data = JSON.parse(jsonString);
    return data;
}


function getDataLocalStorageTrip() {
    let jsonString = localStorage.getItem(TRIP_DATA_KEY);
    let data = JSON.parse(jsonString);
    return data;
}


function getDataLocalStorageAirports() {
    let jsonString = localStorage.getItem(AIRPORTS_DATA_KEY);
    let data = JSON.parse(jsonString);
    return data;
}

let accounts = new AccountList();


if (checkIfAccountDataExistsLocalStorage()) {
    let data = getDataLocalStorageAccount();
    accounts.fromData(data);
    updateLocalStorageAccount(accounts)
} else {
    updateLocalStorageAccount(accounts)
}